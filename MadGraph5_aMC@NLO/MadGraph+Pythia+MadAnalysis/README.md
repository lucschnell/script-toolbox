# MadGraph + Pythia + MadAnalysis


## Purpose
The [`snakemake`](https://snakemake.readthedocs.io/en/stable/) script provided in this folder may be used to generate, shower and analyze events based on existing `MadGraph5_aMC@NLO`, `Pythia` and `MadAnalysis5`  implementations. The script `super_snakefile` should be placed in the `MadGraph5_aMC@NLO/bin` folder, where a `MadGraph5_aMC@NLO` executable for a specific process `process_name` is located. Specifying the `MadAnalysis5` paths in the file `config.yaml` and indicating up to two input parameters in `super_snakefile`, one can then generate a two-dimensional parameter scan for the process `process_name`. The generated events are stored in `process_name/Events`, the `MadAnalysis5` results are placed in `process_name/Madanalysis`. 
