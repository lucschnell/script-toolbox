# Yaml2Mathematica


## Purpose
The Python script `Yaml2Mathematica.py` can be used to convert the data from `HepData` `.yaml` files so that it is interpretable for `Mathematica`. It needs the command line input `folder`, a String stating the absolute path to the folder where the `.yaml` files from `HepData` are located. The script `Yaml2Mathematica.py` then reads these files, takes out the measured as well as the expected number of events and outputs them to the file `results.txt` created in the script directory. 
