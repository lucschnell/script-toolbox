import sys
import os
import shutil
import numpy as np
from sigfig import round
import yaml #Use package pyyaml to read HEPData .yaml files

folder = str(sys.argv[1])

def Yaml2Mathematica(folder):

    # Get script directory
    script_path = os.getcwd()

    #Create output file with results
    try:
        result_file = open(os.path.join(script_path, "results.txt"), "a")
    except:
        print("An error occured while trying to create the results.txt file.")

    #Read all files in folder
    files = [file for file in os.listdir(folder) if os.path.isfile(os.path.join(folder, file))]

    #Go through individual .yaml files
    for file in files:
        if(file == ".DS_Store"):
            continue

        print("Now processing the file " + str(file) + "...")

        #Read .yaml file
        with open(os.path.join(folder, file), "r", encoding='latin1') as stream:
            try:
                data = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print("An error occured while trying to read the .yaml file " + str(file)+". ")

            #################################################
            #Read observed events
            #################################################
            observed_values = []
            observed = data['dependent_variables'][0]['values']

            for obs in observed:
                observed_values.append(str(float(obs['value'])))

            #Output string
            out_string = "Events[observed, "+str(file)+"]={" + ",".join(observed_values) + "}; \n \n"

            #Write output string
            result_file.write(out_string)


            #################################################
            #Read expected events
            #################################################
            expected_values = []
            expected_error_high = []
            expected_error_low = []
            expected = data['dependent_variables'][1]['values']

            for exp in expected:
                expected_values.append(str(float(exp['value'])))

                error_high = 0
                error_low = 0
                for err in exp['errors']:
                    error_high += err['asymerror']['plus']**2
                    error_low += err['asymerror']['minus']**2

                expected_error_high.append(str(np.sqrt(error_high)))
                expected_error_low.append(str(np.sqrt(error_low)))

            #Output string
            out_string = ""
            for i in range(len(expected_values)):
                out_string += "Around[" + expected_values[i]+", {" + expected_error_low[i]+", " + expected_error_high[i]+"}],"

            out_string = "Events[expected, "+str(file)+"]={" + out_string[:-1] + "}; \n \n"

            #Write output string
            result_file.write(out_string)


Yaml2Mathematica(folder)
