import sys
import os
import shutil
import numpy as np


# Finds the histogram files in Output/SAF/.../analysis_name/Histograms, parses them and outputs the histogram data to Output/Results/.../... in a format that can be interpreted by Mathematica.
def FolderHandling(analysis_name, signal_regions_histo, signal_regions_cutflow):
    # Get script directory
    script_path = os.getcwd()

    # Create folder Output/Results (if it does not already exist)
    result_path = os.path.join(script_path, "Output", "Results")
    if not os.path.exists(result_path):
        os.makedirs(result_path)

    # Read folder names in Output/SAF/
    results = [folder for folder in os.listdir(os.path.join(script_path, "Output", "SAF")) if os.path.isdir(os.path.join(script_path, "Output", "SAF", folder))]

    # Create result folders for folder names in Output/SAF
    for result in results:
        result_folder_path = os.path.join(script_path, "Output", "Results", result)
        if not os.path.exists(result_folder_path):
            os.makedirs(result_folder_path)

    # Open individual histogram files
    for result in results:
        try:
            histo_file = open(os.path.join(script_path, "Output", "SAF", result, analysis_name, "Histograms", "histos.saf"), "r")
        except:
            print("An error occured while trying to open the file histos.saf of the events " + result + ".")


        print("\n###########################################################################")
        print("# Processing events " + str(result))
        print("###########################################################################")

        histo_text = histo_file.read()

        for i in range(len(signal_regions_histo)):

            print("\n# Evaluating histogram " + str(signal_regions_histo[i]))

            # Open the corresponding cutflow file
            try:
                cutflow_file = open(os.path.join(script_path, "Output", "SAF", result, analysis_name, "Cutflows", signal_regions_cutflow[i]+".saf"), "r")
                cutflow_text = cutflow_file.read()
            except:
                print("An error occured while trying to open the cutflow file "+ signal_regions_cutflow[i]+".saf of the events" + result + ".")

            # Write histo information
            print("Writing histo information...")
            initial, final, ratio = Cutflow2Ratio(cutflow_text, False)
            result_text = "Initial: " + str(initial) + "\n"
            result_text += "Final: " + str(final) + "\n"
            result_text += "Ratio: " + str(ratio) + "\n \n"
            result_file = open(os.path.join(script_path, "Output", "Results", result, signal_regions_histo[i]+".txt"), "w")
            result_file.write(result_text)


            # Write histo data
            print("Writing histo data...")
            result_text = Histo2Mathematica(histo_text, signal_regions_histo[i], False)
            result_file = open(os.path.join(script_path, "Output", "Results", result, signal_regions_histo[i]+".txt"), "a")
            result_file.write(result_text)

            print("Done.")




def Cutflow2Ratio(cutflow_text, print_info = False):

    # Determine initial sum of weights
    ind_beg = cutflow_text.find("Initial number of events")
    ind_beg = cutflow_text.find("nentries", ind_beg)
    ind_beg = cutflow_text.find("\n", ind_beg)+1

    ind_beg = burn_spaces(cutflow_text, ind_beg)
    ind_end = cutflow_text.find("   ", ind_beg)
    number_1plus = float(cutflow_text[ind_beg:ind_end])
    ind_beg = burn_spaces(cutflow_text, ind_end)

    ind_end = cutflow_text.find("   ", ind_beg)
    number_1minus = float(cutflow_text[ind_beg:ind_end])

    number1 = number_1plus + number_1minus


    # Determine final sum of weights
    ind_beg = cutflow_text.rfind("cut")
    ind_beg = cutflow_text.find("nentries", ind_beg)
    ind_beg = cutflow_text.find("\n", ind_beg)+1

    ind_beg = burn_spaces(cutflow_text, ind_beg)
    ind_end = cutflow_text.find("   ", ind_beg)
    number_2plus = float(cutflow_text[ind_beg:ind_end])
    ind_beg = burn_spaces(cutflow_text, ind_end)

    ind_end = cutflow_text.find("   ", ind_beg)
    number_2minus = float(cutflow_text[ind_beg:ind_end])

    number2 = number_2plus + number_2minus

    if(print_info):
        print("\n\n###########################################")
        print("# Cutflow file info")
        print("###########################################")
        print("Initial sum of weights: " + str(number1))
        print("Final sum of weights: " + str(number2))
        print("Ratio: " + str(number2/number1))

    return number1, number2, number2/number1



# Takes the raw text of a MadAnalysis histo file as input, parses the histograms for the different signal regions and outputs them in a format that can be interpreted by Mathematica.
def Histo2Mathematica(histo_text, signal_region, print_info = False):

    output_string = "HistogramData["+str(signal_region)+"] = {"

    # Print info if desired
    if(print_info):
        print("\n\n###########################################")
        print("# Histogram: " + str(signal_region))
        print("###########################################")

    # Find histogram with name 'signal_region'
    ind_beg = histo_text.find(signal_region)

    # Determine nbins, xmin and xmax
    ind_beg = histo_text.find("xmax", ind_beg)
    ind_beg = histo_text.find("\n", ind_beg)+1

    #nbins
    ind_beg = burn_spaces(histo_text, ind_beg)
    ind_end = histo_text.find("   ", ind_beg)
    nbins = int(histo_text[ind_beg:ind_end])
    ind_beg = burn_spaces(histo_text, ind_end)

    #xmin
    ind_end = histo_text.find("   ", ind_beg)
    xmin = float(histo_text[ind_beg:ind_end])
    ind_beg = burn_spaces(histo_text, ind_end)

    #xax
    ind_end = histo_text.find("   ", ind_beg)
    xmax = float(histo_text[ind_beg:ind_end])
    ind_beg = burn_spaces(histo_text, ind_end)

    #Print info if desired
    if(print_info):
        print("\nHistogram bins")
        print("---------------------------")
        print("nbins: " + str(nbins))
        print("xmin: "+ str(xmin))
        print("xmax: " + str(xmax))

    # Jump to first number entry after 'underflow'
    ind_beg = histo_text.find("underflow", ind_beg)
    ind_beg = histo_text.find("\n", ind_beg)+1
    ind_beg = burn_spaces(histo_text, ind_beg)

    if(print_info):
        print("\nHistogram values")
        print("---------------------------")

    # Read numbers until the '</Data>' tag is reached
    for i in range(nbins):

        # Get the first number
        ind_end = histo_text.find("   ", ind_beg)
        number_plus = float(histo_text[ind_beg:ind_end])

        # Get the second number
        ind_beg = burn_spaces(histo_text, ind_end)
        ind_end = histo_text.find("   ", ind_beg)
        number_minus = float(histo_text[ind_beg:ind_end])

        ind_beg = histo_text.find("\n", ind_beg)+1
        ind_beg = burn_spaces(histo_text, ind_beg)

        # Determine the bin endpoints
        lower = xmin + (xmax-xmin)/nbins*(i)
        upper = xmin + (xmax-xmin)/nbins*(i+1)

        # Add histogram bin to output_string
        bin_value = str(number_plus + number_minus)
        bin_value = bin_value.replace("e+", "*^")
        bin_value = bin_value.replace("e-", "*^-")
        output_string += "{"+str(lower)+", "+str(upper)+", " + bin_value +"}, "

        if(print_info):
            print("["+str(lower) + ", " + str(upper) + "]: " + str(number_plus + number_minus))

    output_string = output_string[:-2] + "}"

    return output_string


def burn_spaces(text, index):
    while(text[index:index+1]==" "):
        index+=1

    return index


FolderHandling("LQ_atlas_0", ["Mvis_lephad", "Mvis_hadhad-high", "Mvis_hadhad-low"], ["hadhad_high", "hadhad_low", "lephad"])
