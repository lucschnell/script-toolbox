# Script Toolbox


## Purpose

In this GitLab repository, I collect scripts that I wrote during my master's thesis and PhD and that proved to be useful. This allows me to easily reuse them in the future. 

As they might be useful for a broader audience, I decided to collect them in a public repository. I do however not provide any warranty that they will work correctly under all circumstances, so do use them with care. Have fun! 

## Contact
If you have any questions, comments or ideas, do not hesitate to contact me: 

- E-mail: [schnell@mpp.mpg.de](mailto:schnell@mpp.mpg.de)
- Skype: [luc.schnell](skype:luc.schnell?chat)


## Credentials
- Logo: script by SBTS from [Noun Project](https://thenounproject.com/browse/icons/term/script/). 
