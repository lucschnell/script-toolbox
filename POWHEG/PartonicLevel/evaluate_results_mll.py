import sys
import os
import shutil
import numpy as np
from sigfig import round


def POWHEG_to_Mathematica(str):
    ind_beg = 0
    ind_end = len(str)
    is_space = True

    while(is_space):
        next_char = str[ind_beg:ind_beg+1]
        if(next_char == " " or next_char == os.linesep):
            ind_beg += 1
        else:
            is_space = False

    is_space = True
    while(is_space):
        next_char = str[ind_end-1:ind_end]
        if(next_char == " " or next_char == os.linesep):
            ind_end -= 1
        else:
            is_space = False

    text = str[ind_beg:ind_end]

    output_text = "{{" + ((text.replace(os.linesep + " ", "},{")).replace("E", "*^")).replace(" ", ",") + "}};"

    return output_text


def Read_and_write_observable(text, folder, obs):

    script_path = os.getcwd()

    #Create folder in 'Results'
    try:
        os.makedirs(os.path.join(script_path, "Results", folder))
    except:
        i = "do nothing"


    output_file = open(os.path.join(script_path, "Results", folder, obs + ".txt"), "a+")
    ind_beg = text.find("# " + obs)
    ind_beg = text.find(os.linesep, ind_beg)+1
    ind_end = text.find("#", ind_beg)-1
    info = text[ind_beg:ind_end]

    output_text = "\n Events[" + folder.replace("_", "") + "] = " + POWHEG_to_Mathematica(info)
    output_file.write(output_text)

    return



#Compiles the results from the different evaluations
def read_folders():

    script_path = os.getcwd()

    #Create folder 'Results'
    try:
        os.makedirs(os.path.join(script_path, "Results"))
    except:
        i = "do nothing"

    #Get names of all folders in script directory
    folders = [name for name in os.listdir(os.path.join(script_path)) if os.path.isdir(os.path.join(script_path, name))]

    for folder in folders:

        #Exclude hidden folders and Results folder
        if(folder[0:1]=="." or folder == "Results"):
            continue

        #Get names of all subfolders in the POWHEG directory
        subfolders = [name for name in os.listdir(os.path.join(script_path, folder, "POWHEG")) if os.path.isdir(os.path.join(script_path, folder, "POWHEG", name))]

        for subfolder in subfolders:

            #Result file
            result_file = open(os.path.join(script_path, folder, "POWHEG", subfolder, "pwgLHEF_analysis.top"), "r")
            text = result_file.read()


            #mll and mlj Spectra
            Read_and_write_observable(text, folder, "mll_spectrum")
            Read_and_write_observable(text, folder, "mlj_spectrum")


read_folders()
