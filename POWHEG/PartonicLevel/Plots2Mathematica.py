import sys
import os
import shutil
import numpy as np
from sigfig import round

plotname = str(sys.argv[1])
weights = int(sys.argv[2])


def translate_to_Mathematica(text):
    text = text[1:]
    text = text.replace(os.linesep + " ", "},"+os.linesep+"{")
    text = text.replace("E", "*^")
    text = text.replace(" ", ",")

    outtext = "{{" + text + "}}" + os.linesep + os.linesep

    return outtext



def read_plot(plotname, weights):

    script_path = os.getcwd()
    out_file = open(os.path.join(script_path,"pwgLHEF_" + plotname +".m"), "a")

    for i in range(weights):
        print("Working on results from weight " + str(i) + "...")
        plot_file = open(os.path.join(script_path,"pwgLHEF_analysis-W" + str(i+1)+".top"), "r")
        text = plot_file.read()

        indbeg = text.find(plotname)
        indbeg = text.find(os.linesep, indbeg)+1

        indend = text.find(os.linesep+os.linesep, indbeg)

        text = text[indbeg:indend]
        outtext = translate_to_Mathematica(text)

        out_file.write("Weight["+str(i)+"] = " + outtext)


read_plot(plotname, weights)
