c Invariant mass bins
      subroutine init_hist
      implicit none
      include  'LesHouches.h'
      include 'pwhg_math.h'
      integer nmllbins
      integer nmljbins
      parameter (nmllbins = 39)
      parameter (nmljbins = 39)
      real*8 mllbins(nmllbins + 1)
      real*8 mljbins(nmljbins + 1)


c ------------------------------
c LS: Define bins
c ------------------------------
      data mllbins/100d0,200d0,300d0,400d0,500d0,600d0,
     1             700d0,800d0,900d0,1000d0,1100d0,
     2             1200d0,1300d0,1400d0,1500d0,1600d0,
     3             1700d0,1800d0,1900d0,2000d0,2100d0,
     4             2200d0,2300d0,2400d0,2500d0,2600d0,
     5             2700d0,2800d0,2900d0,3000d0,3100d0,
     6             3200d0,3300d0,3400d0,3500d0,3600d0,
     7             3700d0,3800d0,3900d0,4000d0/

      data mljbins/100d0,200d0,300d0,400d0,500d0,600d0,
     1             700d0,800d0,900d0,1000d0,1100d0,
     2             1200d0,1300d0,1400d0,1500d0,1600d0,
     3             1700d0,1800d0,1900d0,2000d0,2100d0,
     4             2200d0,2300d0,2400d0,2500d0,2600d0,
     5             2700d0,2800d0,2900d0,3000d0,3100d0,
     6             3200d0,3300d0,3400d0,3500d0,3600d0,
     7             3700d0,3800d0,3900d0,4000d0/

c ------------------------------
c LS: Initialize histograms
c ------------------------------
      call inihists

      call bookup('mll_spectrum', nmllbins, mllbins)
      call bookup('mlj_spectrum', nmljbins, mljbins)
      end

c ================================================
c                  Analysis
c
c LS: here the actual analysis is implemented
c ================================================
      subroutine analysis(dsig0)
      implicit none
      include 'hepevt.h'
      include 'pwhg_math.h'
      include 'PhysPars.h'
      include 'pwhg_bookhist-multi-new.h'
      include 'pwhg_weights.h'
      include 'pwhg_rwl.h'
      include 'LesHouches.h'
c     allow multiweights
      real * 8 dsig0,dsig(1:rwl_maxweights)
      real * 8 costheta
      integer   maxphot,nphot
      parameter (maxphot=2048)
      real * 8 pg(4,maxphot)
      character * 1 cnum(9)
      integer j,k
c     we need to tell to this analysis file which program is running it
      character * 6 WHCPRG
      common/cWHCPRG/WHCPRG
      data WHCPRG/'NLO   '/
      real * 8 pll(4),pl(4),plplus(4),pj(4)
      real * 8 plj(4)
      real * 8 pl03(0:3),plplus03(0:3)
      real * 8 yll,etall,ptll,mll
      real * 8 mlj
      real * 8 dy,deta,delphi,dr
      real * 8 getpt,getdphi,getmass,geteta,gety
      external getpt,getdphi,getmass,geteta,gety
      integer ihep, lept_id
      real * 8 powheginput,dotp
      external powheginput,dotp
      integer vdecaytemp,vdecay2temp
      real * 8 yl,ptl,etal,ylplus,ptlplus,etalplus,ml,mlplus
      logical accepted, jetPresent
      logical includeg, initialg
      real*8 getdeta,getdr,dphi,dr1,dr2
      logical ini,noheavy, pass_cuts
      logical pass_cuts_bar
      logical pass_cuts_end
      data ini/.true./
      save ini

      if (ini) then
          vdecaytemp = lprup(1)-10000
          vdecay2temp = - vdecaytemp

	        if(rwl_num_weights.eq.0) then
             call setupmulti(1)
          else
             call setupmulti(rwl_num_weights+1)
          endif
          ini=.false.
      endif

      dsig=0
      if(rwl_num_weights.eq.0) then
         dsig(1)=dsig0
      else
	       dsig(1) =  dsig0
         dsig(2:rwl_num_weights+1)=rwl_weights(1:rwl_num_weights)
      endif
      if(sum(abs(dsig)).eq.0) return

      pll = (/0,0,0,0/)
      plj = (/0,0,0,0/)
      pl = (/0,0,0,0/)
      pj = (/0,0,0,0/)
      plplus= (/0,0,0,0/)
      jetPresent = .false.

      do ihep=1,nhep

c LS: lepton in final state
         if( idhep(ihep).eq.vdecaytemp  ) then
             if (phep(4,ihep).gt.pl(4)) pl = phep(1:4,ihep)
             lept_id = idhep(ihep)
         endif

c LS: anti-lepton in final state
         if( idhep(ihep).eq.vdecay2temp ) then
             if (phep(4,ihep).gt.plplus(4)) plplus = phep(1:4,ihep)
         endif

c LS: arbitrary jet in final state (if present)
         if ((abs(idhep(ihep)).eq.1.or.abs(idhep(ihep)).eq.2.or.
     #abs(idhep(ihep)).eq.3.or.abs(idhep(ihep)).eq.4.or.
     #abs(idhep(ihep)).eq.5.or.abs(idhep(ihep)).eq.21)
     #.and.phep(1,ihep).ne.0d0) then
             pj = phep(1:4,ihep)
             if(abs(pj(1)**2+pj(2)**2).ge.20d0**2) then
                jetPresent = .true.
             endif
         endif
      enddo

      if(jetPresent) then
          plj = pl + pj
      endif

      pll = pl + plplus

      pl03(0)=pl(4)
      pl03(1:3)=pl(1:3)
      plplus03(0)=plplus(4)
      plplus03(1:3)=plplus(1:3)

      call getyetaptmass(pl,yl,etal,ptl,ml)
      call getyetaptmass(plplus,ylplus,etalplus,ptlplus,mlplus)

      mll = getmass(pll)
      mlj = getmass(plj)

      call filld('mll_spectrum',mll,dsig)

      if(jetPresent) then
          call filld('mlj_spectrum',mlj,dsig)
      endif

      end

      subroutine getyetaptmass(p,y,eta,pt,mass)
      implicit none
      real * 8 p(4),y,eta,pt,mass,pv
      real * 8 gety,getpt,geteta,getmass
      external gety,getpt,geteta,getmass
      y  = gety(p)
      pt = getpt(p)
      eta = geteta(p)
      mass = getmass(p)
      end

      function gety(p)
      implicit none
      real * 8 gety,p(4)
      gety=0.5d0*log((p(4)+p(3))/(p(4)-p(3)))
      end

      function getpt(p)
      implicit none
      real * 8 getpt,p(4)
      getpt = sqrt(p(1)**2+p(2)**2)
      end

      function getmass(p)
      implicit none
      real * 8 getmass,p(4)
      getmass=sqrt(abs(p(4)**2-p(3)**2-p(2)**2-p(1)**2))
      end

      function geteta(p)
      implicit none
      real * 8 geteta,p(4),pv
      real * 8 tiny
      parameter (tiny=1.d-5)
      pv = sqrt(p(1)**2+p(2)**2+p(3)**2)
      if(pv.lt.tiny)then
         geteta=sign(1.d0,p(3))*1.d8
      else
         geteta=0.5d0*log((pv+p(3))/(pv-p(3)))
      endif
      end
