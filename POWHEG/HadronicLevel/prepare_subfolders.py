import sys
import os
import shutil

runname = sys.argv[1]
param1_name = sys.argv[2]
param1_value = sys.argv[3]
param2_name = sys.argv[4]
param2_value = sys.argv[5]

# Changes the parameter with name "param_name" to the value "param_value" in
# the powheg.input file.
def change_param(file_name, param_name, param_value):

    text = ""

    with open(file_name, "r") as file:
        text = file.read()
        param_index = text.find(os.linesep+param_name)
        param_val_beg = 1 + param_index + len(param_name)

        while(text[param_val_beg] == " "):
            param_val_beg += 1

        param_val_end = param_val_beg
        while(text[param_val_end] != " "):
            param_val_end += 1

        text = text[0:param_val_beg] + param_value + text[param_val_end:len(text)];

    with open(file_name, "r+") as file:
        file.write(text)

# Generates an individual subfolder for each parameter combination, copies pwhg_main and powheg.input
# into that subfolder and changes the respective parameters in powheg.input.
def create_subfolders(runname, param1, param2):

    script_path = os.getcwd()
    try:
        os.makedirs(os.path.join(script_path, runname, "POWHEG"))
    except:
        i = "do nothing"
        #print("This run folder already exists.")

    subfolder = "Run_"+str(param1[0])+"_"+str(param1[1])+"_"+str(param2[0])+"_"+str(param2[1])
    try:
        os.makedirs(os.path.join(script_path, runname, "POWHEG", subfolder))
    except:
        i = "do nothing"
	#print("These run subfolders already exist.")


    if(not os.path.exists(os.path.join(script_path, runname, "Results", "results.txt"))):

        try:
            os.makedirs(os.path.join(script_path, runname, "Results"))
        except:
            i = "do nothing"

        open(os.path.join(script_path, runname, "Results", "results.txt"), mode='w').close()

    # Copy all relevant files to the respective subfolders
    shutil.copyfile(os.path.join(script_path, "pwhg_main"), os.path.join(script_path, runname, "POWHEG", subfolder, "pwhg_main"))
    shutil.copyfile(os.path.join(script_path, "powheg.input"), os.path.join(script_path, runname, "POWHEG", subfolder, "powheg.input"))
    shutil.copyfile(os.path.join(script_path, "pythia.cmnd"), os.path.join(script_path, runname, "POWHEG", subfolder, "pythia.cmnd"))
    shutil.copyfile(os.path.join(script_path, "pwg2hepmc"), os.path.join(script_path, runname, "POWHEG", subfolder, "pwg2hepmc"))

    # Change the relevant input parameters
    change_param(os.path.join(script_path, runname, "POWHEG", subfolder, "powheg.input"), param1[0], param1[1])
    change_param(os.path.join(script_path, runname, "POWHEG", subfolder, "powheg.input"), param2[0], param2[1])
    change_param(os.path.join(script_path, runname, "POWHEG", subfolder, "powheg.input"), "MGp", param1[1])


create_subfolders(runname, [param1_name, param1_value], [param2_name, param2_value])
